/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */

import org.newdawn.slick.SlickException;

/**
 * Represents a fighter enemy unit.
 * @author Matthew Rossi <mrossi>
 */
public class Fighter extends Enemy implements MissileState {

	/**
	 * Instantiate a Fighter unit.
	 * 
	 * @param x The x coordinate of the unit, relative to map (pixels).
	 * @param y The y coordinate of the unit, relative to map (pixels).
	 * @throws SlickException Handles exceptions arising from Slick2D library
	 */
	public Fighter(double x, double y) 
		throws SlickException {
		super(x,y,"/units/fighter.png");
		assignStats(24,9,0);
	}
	
	/* (non-Javadoc)
	 * @see MissileState#createMissile(World, int)
	 */
	@Override
	public boolean createMissile(World world, int delta) throws SlickException {
		if (missileTimer <= 0) {
			world.addMissile(new MissileRed(this.x, this.y+img.getHeight()));
			missileTimer = calcFullTimer();
			return true;
		} else {
			missileTimer -= delta;
			return false;
		}
	}

	/**
	 * Update the location of a Fighter character.
	 * 
	 * @param world Reference to an object representing the game world (to check blocking).
	 * @param delta Time passed since last frame (milliseconds).
	 * @throws SlickException SlickException Handles exceptions arising from Slick2D library.
	 */
	public void update(World world, int delta) 
		throws SlickException {		
		Camera cam = world.getCameraRef();
		
		// calculate the map distance to move the unit
		double amount = delta * getSpeed();
		// check if the unit is within the camera boundaries
		checkWindowBoundary(cam);
				
		// move the fighter unit
		moveto(world, this.x, this.y + amount);	
		
		// attempt to make the fighter unit fire a missile
		createMissile(world, delta);
	}
	
	/* (non-Javadoc)
	 * @see MissileState#calcFullTimer(Unit)
	 */
	@Override
	public int calcFullTimer() {
		return 300 - (80 * getFirepower());
	}
}
