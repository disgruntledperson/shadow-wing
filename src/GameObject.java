/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * General representation of any game object
 * @author Matthew Rossi <mrossi>
 */
public abstract class GameObject {
	
	/** Image of the player's ship. */
    protected Image img;
    /** File path of image file of object. */
    protected String img_file;
    
    /** The x coordinate of the object, relative to map (pixels). */
    protected double x;
    /** The y coordinate of the object, relative to map (pixels). */
    protected double y;
    
    /** The original x coordinates of the object, relative to the map (pixels). */
    protected double originalX;
    /** The original x coordinates of the object, relative to the map (pixels). */
    protected double originalY;
    
    /** Status variable recording whether the object is dead or not. */
    protected boolean isDead;
	
	/**
	 * Instantiates the game object.
	 * 
	 * @param x The x coordinate of the object, relative to map (pixels).
	 * @param y The y coordinate of the object, relative to map (pixels).
	 * @param img_file File path to the image representing the object.
	 * @throws SlickException Handles exceptions arising from Slick2D library
	 */
	public GameObject(double x, double y, String img_file) 
		throws SlickException {
		this.x = x;
		this.y = y;
		String image_file = Game.ASSETS_PATH + img_file;
		this.img = new Image(image_file);
		isDead = false;
		
		// Hold original object coordinates
		originalX = x;
		originalY = y;
	}

	/**
	 * Render the game object on the screen.
	 * 
	 * @param g The Slick graphics object, used for drawing.
	 * @param cam Reference to the instantiated camera object.
	 * @throws SlickException Handles exceptions arising from Slick2D library
	 */
	
	protected void render(Graphics g, Camera cam) 
		throws SlickException {
		double xWindow, yWindow;
		// Calculate window coordinates of unit
		xWindow = x-cam.getLeft();
		yWindow = y-cam.getTop();
		// If within bounds, render the unit
		if ((xWindow > 0 && xWindow < Game.playwidth()) && 
				(yWindow > 0 && yWindow < Game.playheight())) {
			img.drawCentered((int)xWindow, (int)yWindow);
		}
	}

	/**
	 * Return the image of the object's ship.
	 * 
	 * @return The reference to the instantiated Image object.
	 */
	public Image getImg() {
		return img;
	}

	/**
	 * Determine whether two game objects are colliding.
	 * 
	 * @param second Reference to another game object to compare.
	 * @return Boolean value determining if two game objects are colliding.
	 */
	protected boolean isColliding(GameObject second) {
		if (this == second)
			return false;
				
		int aWidthFromCentre = img.getWidth()/2;
		int aHeightFromCentre = img.getHeight()/2;
		
		int bWidthFromCentre = second.getImg().getWidth()/2;
		int bHeightFromCentre = second.getImg().getHeight()/2;
				
		double aMinX = this.x - aWidthFromCentre;
		double aMinY = this.y - aHeightFromCentre;
		double aMaxX = this.x + aWidthFromCentre;
		double aMaxY = this.y + aHeightFromCentre;
		
		double bMinX = second.getX() - bWidthFromCentre;
		double bMinY = second.getY() - bHeightFromCentre;
		double bMaxX = second.getX() + bWidthFromCentre;
		double bMaxY = second.getY() + bHeightFromCentre;
		
		// Determine if objects are colliding
		if (aMaxX < bMinX) return false;
		if (aMinX > bMaxX) return false;
		if (aMaxY < bMinY) return false;
		if (aMinY > bMaxY) return false;
				
		return true;
	}

	/**
	 * Return the x coordinate of the object.
	 * 
	 * @return The x coordinate of the object, relative to map (pixels).
	 */
	public double getX() {
		return x;
	}

	/**
	 * Return the y coordinate of the object.
	 * 
	 * @return The x coordinate of the object, relative to map (pixels).
	 */
	public double getY() {
		return y;
	}

	/**
	 * Manually set the object coordinates.
	 * 
	 * @param x The x coordinate of the object, relative to map (pixels).
	 * @param y The y coordinate of the object, relative to map (pixels).
	 */
	public void setCord(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Determine whether the object is dead
	 * 
	 * @return Boolean variable of the dead/alive status of the game object.
	 */
	public boolean isDead() {
		return isDead;
	}

	/**
	 * Change the dead/alive status of the game object.
	 * 
	 * @param isDead Boolean variable of the dead/alive status of the game object.
	 */
	public void setDead(boolean isDead) {
		this.isDead = isDead;
	}
	
	/**
	 * Check whether the object is within the camera boundary
	 * 
	 * @param cam Reference to the camera object.
	 */
	protected void checkWindowBoundary(Camera cam) {
		if (x < cam.getLeft())
            x = cam.getLeft();
        if (x > (cam.getRight() - 1))
            x = cam.getRight() - 1;
        if (y < cam.getTop())
            y = cam.getTop();
        if (y > (cam.getBottom() - Panel.PANEL_HEIGHT - 1))
            y = cam.getBottom() - Panel.PANEL_HEIGHT - 1;
	}
	
}
