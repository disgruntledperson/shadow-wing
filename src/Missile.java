/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */
import org.newdawn.slick.SlickException;

/**
 * Representation of missiles launched by characters in the game. 
 * @author Matthew Rossi <mrossi>
 *
 */
public abstract class Missile extends GameObject {
		
	/**
	 * Instantiate a missile.
	 * 
	 * @param x The x coordinate of the missile, relative to map (pixels).
	 * @param y The y coordinate of the missile, relative to map (pixels).
	 * @param img_file File path of image file of missile.
	 * @throws SlickException Handles exceptions arising from Slick2D library
	 */
	public Missile(double x, double y, String img_file) 
			throws SlickException {
			super(x,y,img_file);
	}
	
	/**
	 * Method definition to determine automatic movement of a missile object.
	 * 
	 * @param world Object reference to the game world.
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public abstract void autoMove(World world, int delta);
	
	/**
	 * Determine whether a missile is 'dead'.
	 * 
	 * @param world Object reference to the game world.
	 */
	protected void determineDeath(World world) {
		Camera cam = world.getCameraRef();
		
		if (world.terrainBlocks(this.x, this.y)) world.removeMissile(this);
		if (this.getY() >= cam.getBottom()-Panel.PANEL_HEIGHT) world.removeMissile(this);
		if (this.getY() < cam.getTop()) world.removeMissile(this);
	}

	/**
	 * Speed of missile (pixels per second)
	 * 
	 * @return The number of pixels the missile automatically moves per millisecond.
	 */
	public double getSpeed() {
		return 0.7;
	}
	
	/**
	 * Damage stat of a missile.
	 * 
	 * @return The damage a missile inflicts on a unit.
	 */
	public int getShieldDamage() {
		return 8;
	}
	
	/**
	 * Determine whether a missile is in the same location as a game object
	 * 
	 * @param obj Object reference of a GameObject
	 * @return True/false of whether the map coordinates of the missile and the object are equal
	 */
	public boolean equals(Object obj) {
		GameObject unit = (GameObject)obj;
		
		if ((this.getX() == unit.getX()) && (this.getY() == unit.getY()))
			return true;
		
		return false;
	}
}
