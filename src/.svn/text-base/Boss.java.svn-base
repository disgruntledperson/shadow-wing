/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */

import org.newdawn.slick.SlickException;

/**
 * Represents a Boss enemy unit.
 * @author Matthew Rossi <mrossi>
 */
public class Boss extends Enemy implements MissileState {

	/* Determines direction of boss movement */
	private boolean movesRight = false;
	/* Boss movement boundary constants */
	private final int LEFT_BOUNDARY = 1072;
	private final int RIGHT_BOUNDARY = 1520;
		
	/**
	 * Instantiate a Boss unit.
	 * 
	 * @param x The x coordinate of the unit, relative to map (pixels).
	 * @param y The y coordinate of the unit, relative to map (pixels).
	 * @throws SlickException Handles exceptions arising from Slick2D library
	 */
	public Boss(double x, double y) 
		throws SlickException {
		super(x,y,"/units/boss.png");
		assignStats(240,100,3);
	}
	
	/* (non-Javadoc)
	 * @see MissileState#createMissile(World, int)
	 */
	@Override
	public boolean createMissile(World world, int delta) throws SlickException {
		if (missileTimer <= 0) {
			world.addMissile(new MissileRed(this.x, this.y+img.getHeight()));
			missileTimer = calcFullTimer();
			return true;
		} else {
			missileTimer -= delta;
			return false;
		}
	}

	/**
	 * Update the location of a Boss character.
	 * 
	 * @param world Reference to an object representing the game world (to check blocking).
	 * @param delta Time passed since last frame (milliseconds).
	 * @throws SlickException SlickException Handles exceptions arising from Slick2D library.
	 */
	public void update(World world, int delta) 
		throws SlickException {
		Camera cam = world.getCameraRef();
		
		// calculate the map distance to move the unit
		double amount = delta * getSpeed();
		// check if the unit is within the camera boundaries
		checkWindowBoundary(cam);
		
		// determine which horizontal direction to move the boss
		if (movesRight) {
			this.x += amount;
		} else {
			this.x -= amount;
		}
		
		// change the horizontal direction when the boss has reached a certain coordinate
		if (x <= LEFT_BOUNDARY)
			movesRight = true;
		else if (x >= RIGHT_BOUNDARY)
			movesRight = false;
		
		// attempt to make the boss fire a missile
		createMissile(world, delta);
	}
	
	/* (non-Javadoc)
	 * @see MissileState#calcFullTimer(Unit)
	 */
	@Override
	public int calcFullTimer() {
		return 300 - (80 * getFirepower());
	}

}
