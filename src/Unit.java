/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */
import org.newdawn.slick.SlickException;

/**
 * Class representing any character of the game.
 * @author Matthew Rossi <mrossi> 
 */
public abstract class Unit extends GameObject {

	/** Number of shield points equivalent to a full unit shield. */
	protected int fullShield;
	/** The current number of shield points remaining in a unit. */
	protected int shield;
	/** The amount of damage the unit causes when it collides with another unit. */
	protected int damage;
	/** The numerical factor that determines how rapidly the unit can fire missiles. */
	protected int firepower;
	/** Number of milliseconds until the next opportunity for a missile to be launched by a character. */
	protected int missileTimer;
		
	/**
	 * Instantiate a game unit.
	 * 
	 * @param x The x coordinate of the unit, relative to map (pixels).
	 * @param y The y coordinate of the unit, relative to map (pixels).
	 * @param img_file File path to the unit representing the object.
	 * @throws SlickException Handles exceptions arising from Slick2D library
	 */
	public Unit(double x, double y, String img_file) 
			throws SlickException {
			super(x,y,img_file);
	}

	/**
	 * Move a unit to a particular location.
	 * 
	 * @param world Reference to an object representing the game world (to check blocking).
	 * @param x The x coordinate of the object, relative to map (pixels).
	 * @param y The y coordinate of the object, relative to map (pixels).
	 */
    protected void moveto(World world, double x, double y) {
    	// calculate horizontal and vertical distances from centre to the extreme ends of the image
    	int xWidth = img.getWidth()/2;
    	int yWidth = img.getHeight()/2;
    	
    	// calculate the map coordinates of the horizontal and vertical ends of the image
    	double xLeft = x - xWidth;
    	double xRight = x + xWidth;
    	double yTop = y - yWidth;
    	double yBottom = y + yWidth;
    	
    	boolean left = true, right = true, top = true, bottom = true;
    	
    	// check vertically blocked
    	top = !world.terrainBlocks(x, yTop);
    	bottom = !world.terrainBlocks(x, yBottom);
    	
    	// check horizontally blocked
    	left = !world.terrainBlocks(xLeft, y);
    	right = !world.terrainBlocks(xRight, y);
    	
    	// if no block
    	if (left && right && top && bottom) {
    		this.x = x;
    		this.y = y;
    	}
    	// left block
    	if (!left && right && top && bottom) {
    		this.y = y;
    	}
    	// right block
    	if (left && !right && top && bottom) {
    		this.y = y;
    	}
    	// top block
    	if (left && right && !top && bottom) {
    		this.x = x;
    	}
    	// bottom block
    	if (left && right && top && !bottom) {
    		this.x = x;
    	}   
    	// inflict self-damage
    	if (!(left && right && top && bottom)) {
    		adjustShield(this);
    	}
    	
    }
	
    /**
     * Assign unit states to the object.
     * 
     * @param fullShield Number of shield points equivalent to a full unit shield.
     * @param damage The amount of damage the unit causes when it collides with another unit. 
     * @param firepower Determines how rapidly the unit can fire missiles.
     */
	protected void assignStats(int fullShield, int damage, int firepower) {
		this.fullShield = fullShield;
		this.shield = fullShield;
		this.damage = damage;
		this.firepower = firepower;
	}
	
	/**
	 * Return the value representing a full unit shield.
	 * 
	 * @return Number of shield points equivalent to a full unit shield.
	 */
	public int getFullShield() {
		return fullShield;
	}

	/**
	 * Return the current shield value of a unit.
	 * 
	 * @return Number of shield points remaining in a unit.
	 */
	public int getShield() {
		return shield;
	}

	/**
	 * Return the current firepower of a unit.
	 * 
	 * @return An integer determining how rapidly the unit can fire missiles.
	 */
	public int getFirepower() {
		return firepower;
	}
	
	/**
	 * Return the potential damage a unit can cause.
	 * 
	 * @return The amount of damage the unit causes when it collides with another unit.
	 */
	public int getDamage() {
		return damage;
	}

	/**
	 * Adjust the unit shield value following a collision with a game unit.
	 * 
	 * @param second Reference to the other colliding unit.
	 */
	public void adjustShield(Unit second) {
		if (shield > 0)
			shield -= second.getDamage();
		else
			isDead = true;
	}
	
	/**
	 * Adjust the unit shield value following a collision with a missile.
	 * 
	 * @param missile Reference to the other colliding missile.
	 */
	public void adjustShield(Missile missile) {
		if (shield > 0)
			shield -= missile.getShieldDamage();
		else
			isDead = true;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Unit [fullShield=" + fullShield + ", shield=" + shield
				+ ", damage=" + damage + ", firepower=" + firepower + ", x="
				+ x + ", y=" + y + ", isDead=" + isDead + "]";
	}
	
}
