/* SWEN20003 Object Oriented Software Development
 * Space Game Engine (Sample Project)
 * Author: Matt Giuca <mgiuca>
 * Modified: Matthew Rossi <mrossi>, October 2013
 */

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/** The ship which the player controls.
 */
public class Player extends Unit implements MissileState
{
	/**
	 * Creates a new Player.
	 * @param x The player's start location (x, pixels).
	 * @param y The player's start location (y, pixels).
	 * @throws SlickException Handles exceptions arising from Slick2D library.
	 */
	public Player(double x, double y) throws SlickException {
		super(x, y, "/units/player.png");
		assignStats(100,10,0);
	}

	/** The x coordinate of the player, relative to map (pixels). */
    public double getX()
    {
        return x;
    }

    /** The y coordinate of the player, relative to map (pixels). */
    public double getY()
    {
        return y;
    }

    /** Draw the Player to the screen at the correct place.
     * @param g The current Graphics context.
     * @param cam Camera used to render the current screen.
     */
    public void render(Graphics g, Camera cam)
    {
        // Calculate the player's on-screen location from the camera
        int screen_x, screen_y;
        screen_x = (int) (this.x - cam.getLeft());
        screen_y = (int) (this.y - cam.getTop());
        img.drawCentered(screen_x, screen_y);
    }

    /** Move the player automatically forwards, as well as (optionally) in a
     * given direction. Prevents the player from moving onto blocking terrain,
     * or outside of the screen (camera) bounds.
     * @param world The world the player is on (to check blocking).
     * @param cam The current camera (to check blocking).
     * @param dir_x The player's movement in the x axis (-1, 0 or 1).
     * @param dir_y The player's movement in the y axis (-1, 0 or 1).
     * @param delta Time passed since last frame (milliseconds).
     */
    public void update(World world, Camera cam, double dir_x, double dir_y,
        int delta)
        throws SlickException {
        /* Calculate the amount to move in each direction, based on speed */
        double amount = delta * getSpeed();
        /* The new location */
        double x = this.x + dir_x * amount;
        double y = this.y + dir_y * amount;
        if (!world.reachedTop())
            y -= delta * getAutoSpeed();
        else
        	checkIfWin();
        if (y < 0)
        	y = 0;
        checkWindowBoundary(cam);
        
        if (this.getY() <= cam.getTop())
        	y = cam.getTop();
        else
        	moveto(world, x, y);
    }
    
    /**
     * Automatically move player following the defeat of the boss.
     * @param line Linear model of the path that the player takes to freedom.
     * @param delta Time passed since last frame (milliseconds).
     */
    public void update(LinearModel line, int delta) 
    	throws SlickException {
    	/* Calculate the amount to move in each direction, based on speed */
        double amount = delta * getSpeed();
        /* The new location */
        if (y >= -(img.getHeight()/2)) {
        	this.y -= amount;
        	this.x = line.getX(y);
        }
        checkIfWin();
    }
    
    /** Perform post-player victory housekeeping  */
    private void checkIfWin() 
    	throws SlickException {
    	// Calculate map coordinate of bottom of player
    	double playerBottom = y + (img.getHeight()/2);
    	// Close program if player has won
    	if (playerBottom <= 0)
        	System.exit(0);
    }
            
    /** The number of pixels the player may move per millisecond. */
    private double getSpeed()
    {
        return 0.4;
    }

    /** The number of pixels the player automatically moves per millisecond.
     */
    private double getAutoSpeed()
    {
        return 0.25;
    }

	/* (non-Javadoc)
	 * @see MissileState#createMissile(World)
	 */
	@Override
	public boolean createMissile(World world, int delta) 
			throws SlickException {
		if (missileTimer <= 0) {
			world.addMissile(new MissileBlue(this.x, this.y-img.getHeight()));
			missileTimer = calcFullTimer();
			return true;
		} else {
			missileTimer -= delta;
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see MissileState#calcFullTimer(Unit)
	 */
	@Override
	public int calcFullTimer() {
		return 300 - (80 * getFirepower());
	}
	
	/**
	 * Reset player to a predefined checkpoint based on its coordinates
	 * 
	 * @param checkpoints An array of y-coordinates a player can teleport to after death.
	 */
	public void unkill(int[] checkpoints) {
		shield = fullShield;
		x = 1296;
		
		// Determine which checkpoints the player has already passed
		for (int i=1; i < checkpoints.length; i++) {
			if (y > checkpoints[i]) {
				y = checkpoints[i-1];
				break;
			}
		}
		
		// Resurrect the player from the dead
		setDead(false);
	}
    
}
