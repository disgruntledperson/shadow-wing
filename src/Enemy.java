/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */

import org.newdawn.slick.SlickException;

/**
 * General representation of an Enemy unit.
 * @author Matthew Rossi <mrossi>
 */
public abstract class Enemy extends Unit {
	
	/**
	 * Instantiate an enemy unit.
	 * 
	 * @param x The x coordinate of the unit, relative to map (pixels).
	 * @param y The y coordinate of the unit, relative to map (pixels).
	 * @param img_file File path to the unit representing the object.
	 * @throws SlickException Handles exceptions arising from Slick2D library
	 */
	public Enemy(double x, double y, String img_file) 
			throws SlickException {
			super(x,y,img_file);
	}
	
	/**
	 * Method definition to update the location of an enemy unit.
	 * 
	 * @param world Reference to an object representing the game world (to check blocking).
	 * @param delta Time passed since last frame (milliseconds).
	 * @throws SlickException SlickException Handles exceptions arising from Slick2D library.
	 */
	public abstract void update(World world, int delta) throws SlickException;
	
	/**
	 * Speed of an enemy (pixels per second)
	 * 
	 * @return The number of pixels an enemy automatically moves per millisecond.
	 */
	public double getSpeed() {
		return 0.2;
	}

}
