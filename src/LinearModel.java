/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */

/**
 * Represents a linear model.
 * @author Matthew Rossi <mrossi>
 */
public class LinearModel {

	/* Calculated constants of model */
	private double x1; 
	private double y1; 
	private double x2; 
	private double y2;
	private double m;
	
	/**
	 * Instantiate a linear model.
	 * 
	 * @param x1 x coordinate for one graph point.
	 * @param y1 y coordinate for the same graph point as x1.
	 */
	public LinearModel(double x1, double y1) {
		this.x1 = x1;
		this.y1 = y1;
		
		this.x2 = 1296;
		this.y2 = 0;
		
		if ((x2-x1) == 0)
			this.m = 0;
		else
			this.m = (y2-y1)/(x2-x1);
	}
	
	/**
	 * Calculate the x coordinate given a y coordinate.
	 * 
	 * @param y Any y coordinate.
	 * @return The x coordinate corresponding to the given y coordinate.
	 */
	public double getX(double y) {
		double frac = ((y-y1)*(x2-x1))/(y2-y1);
		
		return x1+frac;
	}

	/**
	 * Calculate the y coordinate given an x coordinate.
	 * 
	 * @param x Any x coordinate.
	 * @return The y coordinate corresponding to the given y coordinate.
	 */
	public double getY(double x) {
		return (m*(x-x1))+y1;
	}
	
}
