/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */

import org.newdawn.slick.SlickException;

/**
 * Represents a drone enemy unit.
 * @author Matthew Rossi <mrossi>
 */
public class Drone extends Enemy {

	/**
	 * Instantiate a Drone unit.
	 * 
	 * @param x The x coordinate of the unit, relative to map (pixels).
	 * @param y The y coordinate of the unit, relative to map (pixels).
	 * @throws SlickException Handles exceptions arising from Slick2D library
	 */
	public Drone(double x, double y) 
		throws SlickException {
		super(x,y,"/units/drone.png");
		assignStats(16,8,0);
	}
	
	/**
	 * Update the location of a Drone character.
	 * 
	 * @param world Reference to an object representing the game world (to check blocking).
	 * @param delta Time passed since last frame (milliseconds).
	 * @throws SlickException SlickException Handles exceptions arising from Slick2D library.
	 */
	public void update(World world, int delta) 
		throws SlickException {
		Camera cam = world.getCameraRef();
		Player player = world.getPlayerRef();
		
		// calculate the map distance to move the unit
		double amount = delta * getSpeed();
		// calculate the distance between the player and the drone
		double distX = player.getX() - this.x;
		double distY = player.getY() - this.y;
		
		// check whether the drone is within the camera boundaries
		checkWindowBoundary(cam);
		
		// calculate the total distance
		double distTotal = Math.sqrt(Math.pow(distX, 2) + Math.pow(distY, 2));
		
		// calculate the new drone coordinates
		double dx = this.x + (distX/distTotal)*amount;
		double dy = this.y + (distY/distTotal)*amount;
		
		// move the drone
		moveto(world, dx, dy);
		
	}
	
}
