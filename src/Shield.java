/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */

import org.newdawn.slick.SlickException;

/**
 * Represents a Shield item.
 * @author Matthew Rossi <mrossi>
 */
public class Shield extends Item {
	
	/**
	 * Instantiates a Shield item.
	 * 
	 * @param x The x coordinate of the item, relative to map (pixels).
	 * @param y The y coordinate of the item, relative to map (pixels).
	 * @throws SlickException Handles exceptions arising from Slick2D library.
	 */
	public Shield(double x, double y)
		throws SlickException {
		super(x,y,"/items/shield.png");
	}
	
	/**
	 * Increase the shield resistance of the player upon collision with the item.
	 * 
	 * @param player Reference to a player object.
	 */
	public void update(Player player) {
		player.shield += getIncrement();
		player.fullShield += getIncrement();
	}
	
	/**
	 * Shield capacity increase value.
	 * 
	 * @return Integer value of the shield points increase of the player upon collision.
	 */
	private int getIncrement() {
		return 40;
	}
}
