/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */

import org.newdawn.slick.SlickException;

/**
 * Instantiate a game item.
 * @author Matthew Rossi <mrossi>
 */
public abstract class Item extends GameObject {

	/**
	 * Instantiates the game item.
	 * 
	 * @param x The x coordinate of the item, relative to map (pixels).
	 * @param y The y coordinate of the item, relative to map (pixels).
	 * @param img_file File path to the image representing the item.
	 * @throws SlickException Handles exceptions arising from Slick2D library.
	 */
	public Item(double x, double y, String img_file) 
		throws SlickException {
		super(x,y,img_file);
	}

	/**
	 * Update player stats upon collision with the item.
	 * 
	 * @param player Reference to a player object.
	 */
	public abstract void update(Player player);
	
}
