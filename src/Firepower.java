/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */

import org.newdawn.slick.SlickException;

/**
 * Represents a Firepower item.
 * @author Matthew Rossi <mrossi>
 */
public class Firepower extends Item {

	/**
	 * Instantiates a Firepower item.
	 * 
	 * @param x The x coordinate of the item, relative to map (pixels).
	 * @param y The y coordinate of the item, relative to map (pixels).
	 * @throws SlickException Handles exceptions arising from Slick2D library.
	 */
	public Firepower(double x, double y) 
		throws SlickException {
		super(x,y,"/items/firepower.png");
	}
	
	/**
	 * Increase the firepower of the player upon collision with the item.
	 * 
	 * @param player Reference to a player object.
	 */
	public void update(Player player) {
		if (player.getFirepower() < 3)
			player.firepower++;
	}

}
