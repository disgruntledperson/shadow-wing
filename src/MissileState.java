/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */

import org.newdawn.slick.SlickException;

/**
 * Interface to implement methods for managing missiles in unit classes.
 * @author Matthew Rossi <mrossi>
 */
public interface MissileState {
	/**
	 * Create a missile object that is fired from a unit.
	 * 
	 * @param world Reference to an object representing the game world (to check blocking).
	 * @param delta Time passed since last frame (milliseconds).
	 * @return Boolean confirming whether a missile has been successfully created.
	 * @throws SlickException Handles exceptions arising from Slick2D library.
	 */
	boolean createMissile(World world, int delta) throws SlickException;
	
	/**
	 * Calculate the value of a full missile firing timer of a unit.
	 * 
	 * @return Time in milliseconds representing a full waiting period before firing the next missile.
	 */
	int calcFullTimer();
}
