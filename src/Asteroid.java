/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */

import org.newdawn.slick.SlickException;

/**
 * Represents an asteroid enemy unit.
 * @author Matthew Rossi <mrossi>
 */
public class Asteroid extends Enemy {

	
	/**
	 * Instantiate an Asteroid unit.
	 * 
	 * @param x The x coordinate of the unit, relative to map (pixels).
	 * @param y The y coordinate of the unit, relative to map (pixels).
	 * @throws SlickException Handles exceptions arising from Slick2D library
	 */
	public Asteroid(double x, double y) 
		throws SlickException {
		super(x,y,"/units/asteroid.png");
		assignStats(24,12,0);
	}
	
	/**
	 * Update the location of an Asteroid character.
	 * 
	 * @param world Reference to an object representing the game world (to check blocking).
	 * @param delta Time passed since last frame (milliseconds).
	 * @throws SlickException SlickException Handles exceptions arising from Slick2D library.
	 */
	public void update(World world, int delta) {
		Camera cam = world.getCameraRef();
		
		// calculate the map distance to move the unit
		double amount = delta * getSpeed();
		// check if the unit is within the camera boundaries
		checkWindowBoundary(cam);
		
		// move the asteroid
		moveto(world, this.x, this.y + amount);
	}
		
}
