/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */

import org.newdawn.slick.SlickException;

/**
 * Represents a repair item.
 * @author Matthew Rossi <mrossi>
 */
public class Repair extends Item {

	/**
	 * Instantiates a Repair item.
	 * 
	 * @param x The x coordinate of the item, relative to map (pixels).
	 * @param y The y coordinate of the item, relative to map (pixels).
	 * @throws SlickException Handles exceptions arising from Slick2D library.
	 */
	public Repair(double x, double y)
		throws SlickException {
		super(x,y,"/items/repair.png");
	}
	
	/**
	 * Repair the player (set shield value to fullShield) upon collision with the item.
	 * 
	 * @param player Reference to a player object.
	 */
	public void update(Player player) {
		player.shield = player.fullShield;
	}
	
}
