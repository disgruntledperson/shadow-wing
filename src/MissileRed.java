/**
 * SWEN20003 Object Oriented Software Development
 * Shadow Wing
 */

import org.newdawn.slick.SlickException;

/**
 * Represents a red missile.
 * @author Matthew Rossi <mrossi>
 */

public class MissileRed extends Missile {

	/**
	 * Instantiate a blue missile.
	 * 
	 * @param x The x coordinate of the missile, relative to map (pixels).
	 * @param y The y coordinate of the missile, relative to map (pixels).
	 * @throws SlickException Handles exceptions arising from Slick2D library
	 */
	public MissileRed(double x, double y)
		throws SlickException {
		super(x,y,"/units/missile-enemy.png");
		y += img.getHeight();
	}

	/**
	 * Determine automatic movement of a blue missile object.
	 * 
	 * @param world Object reference to the game world.
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void autoMove(World world, int delta) {
		Camera cam = world.getCameraRef();
		double amount = delta * getSpeed();
		
		// check if missile exists within the window boundary
		checkWindowBoundary(cam);
		// determine whether a missile needs to be killed
		determineDeath(world);
		
		// move missile by the calculated amount
		this.y += Math.abs(amount);
	}
	
}
