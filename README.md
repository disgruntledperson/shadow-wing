A 2D First Person Shooter game written in Java using the [Slick2D](http://slick.ninjacave.com/) framework to fulfil the assessment requirements of [SWEN20003](https://handbook.unimelb.edu.au/view/2013/SWEN20003).

_As part of submission requirements, revision control was handled through a Subversion repository through IVLE._

**Legal Notice**

The graphics included with the package (tiles.png, panel.png, units/\*, items/\*) are primarily derived from copyrighted works from the games Chromium B.S.U. (licensed under the Artistic License), The Battle for Wesnoth (licensed under the GNU General Public License, version 2 or later), and other works. You may redistribute them, but only if you agree to the license terms.
For more information, see assets/README.txt, included in the supplied package.
